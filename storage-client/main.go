package main

import (
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"log"
	"net"
	"storage-client/config"
	mapTask "storage-client/server"
	pb "storage-client/task/delivery"
	"storage-client/task/repository/redis"
	"storage-client/task/usecases"
)

func main() {
	if err := config.Init(); err != nil {
		log.Fatal(err.Error())
	}
	lis, err := net.Listen("tcp", viper.GetString("app.address"))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer([]grpc.ServerOption{}...)

	server := &mapTask.TaskServer{
		UseCase: usecases.NewTaskUseCase(redis.NewTaskRepository(viper.GetString("redis.address"))),
	}

	pb.RegisterStorageServer(grpcServer, server)

	log.Printf("starting server on address %s", lis.Addr().String())

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
}
