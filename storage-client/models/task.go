package models

type Task struct {
	ID             string   `json:"id"`
	IncomingValues []string `json:"incoming_values"`
	OutgoingValues []string `json:"outgoing_values"`
	FunctionName   string   `json:"function_name"`
}
