package server

import (
	"context"
	"log"
	"storage-client/task"
	pb "storage-client/task/delivery"
)

type TaskServer struct {
	pb.UnimplementedStorageServer
	UseCase task.UseCase
}

func (s *TaskServer) CreateTask(ctx context.Context, r *pb.CreateTaskRequest) (*pb.CreateTaskResponse, error) {
	resp := &pb.CreateTaskResponse{}
	log.Printf("create task request: %v", r)
	id, err := s.UseCase.CreateTask(ctx, r.Task.IncomingValues, r.Task.OutgoingValues, r.Task.FunctionName)
	if err != nil {
		return resp, err
	}
	resp.Id = id
	return resp, nil
}

func (s *TaskServer) GetTask(ctx context.Context, r *pb.GetTaskRequest) (*pb.GetTaskResponse, error) {
	resp := &pb.GetTaskResponse{}
	log.Printf("get task request: %v", r)
	taskEntity, err := s.UseCase.GetTask(ctx, r.Id)
	if err != nil {
		return resp, err
	}
	resp.Task = &pb.Task{}
	resp.Task.Id = taskEntity.ID
	resp.Task.IncomingValues = taskEntity.IncomingValues
	resp.Task.OutgoingValues = taskEntity.OutgoingValues
	resp.Task.FunctionName = taskEntity.FunctionName
	return resp, nil
}

func (s *TaskServer) UpdateTask(ctx context.Context, r *pb.UpdateTaskRequest) (*pb.UpdateTaskResponse, error) {
	resp := &pb.UpdateTaskResponse{}
	log.Printf("update task request: %v", r)
	taskEntity, err := s.UseCase.UpdateTask(ctx, r.Id, r.OutgoingValues)
	if err != nil {
		return resp, err
	}
	resp.Task = &pb.Task{}
	resp.Task.Id = taskEntity.ID
	resp.Task.IncomingValues = taskEntity.IncomingValues
	resp.Task.OutgoingValues = taskEntity.OutgoingValues
	resp.Task.FunctionName = taskEntity.FunctionName
	return resp, nil
}

func (s *TaskServer) DeleteTask(ctx context.Context, r *pb.DeleteTaskRequest) (*pb.DeleteTaskResponse, error) {
	resp := &pb.DeleteTaskResponse{}
	log.Printf("delete task request: %v", r)
	err := s.UseCase.DeleteTask(ctx, r.Id)
	if err != nil {
		return resp, err
	}
	return resp, nil
}
