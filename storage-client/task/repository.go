package task

import (
	"context"
	"storage-client/models"
)

type Repository interface {
	CreateTask(ctx context.Context, task *models.Task) (string, error)
	GetTask(ctx context.Context, id string) (*models.Task, error)
	UpdateTask(ctx context.Context, id string, outgoingValues []string) (*models.Task, error)
	DeleteTask(ctx context.Context, id string) error
}
