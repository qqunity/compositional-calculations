package redis

import (
	"context"
	"encoding/json"
	"github.com/go-redis/redis/v8"
	"github.com/google/uuid"
	"storage-client/models"
	"storage-client/task"
)

type TaskRepository struct {
	client *redis.Client
}

func NewTaskRepository(addr string) *TaskRepository {
	return &TaskRepository{
		client: redis.NewClient(&redis.Options{
			Addr:     addr,
			Password: "",
			DB:       0,
		}),
	}
}

func (r *TaskRepository) CreateTask(ctx context.Context, taskEntity *models.Task) (string, error) {
	var id string
	for {
		id = uuid.New().String()
		_, err := r.client.Get(ctx, id).Result()
		if err == redis.Nil {
			break
		}
	}
	taskEntity.ID = id
	taskJson, err := json.Marshal(*taskEntity)
	if err != nil {
		return "", err
	}
	err = r.client.Set(ctx, id, taskJson, 0).Err()
	if err != nil {
		return "", err
	}
	return id, nil
}

func (r *TaskRepository) GetTask(ctx context.Context, id string) (*models.Task, error) {
	taskRaw, err := r.client.Get(ctx, id).Result()
	if err != nil {
		return nil, err
	}
	taskEntity := &models.Task{}
	err = json.Unmarshal([]byte(taskRaw), taskEntity)
	if err != nil {
		return nil, err
	}
	return taskEntity, nil
}

func (r *TaskRepository) UpdateTask(ctx context.Context, id string, outgoingValues []string) (*models.Task, error) {
	taskRaw, err := r.client.Get(ctx, id).Result()
	if err == redis.Nil {
		return nil, task.ErrTaskNotFound
	} else if err != nil {
		return nil, err
	}
	taskEntity := &models.Task{}
	err = json.Unmarshal([]byte(taskRaw), taskEntity)
	if err != nil {
		return nil, err
	}
	taskEntity.OutgoingValues = outgoingValues
	taskJson, err := json.Marshal(*taskEntity)
	if err != nil {
		return nil, err
	}
	err = r.client.Set(ctx, id, taskJson, 0).Err()
	if err != nil {
		return nil, err
	}
	return taskEntity, nil
}

func (r *TaskRepository) DeleteTask(ctx context.Context, id string) error {
	_, err := r.client.Get(ctx, id).Result()
	if err == redis.Nil {
		return task.ErrTaskNotFound
	} else if err != nil {
		return err
	}
	_, err = r.client.Del(ctx, id).Result()
	if err != nil {
		return err
	}
	return nil
}
