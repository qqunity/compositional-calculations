package usecases

import (
	"context"
	"storage-client/models"
	"storage-client/task"
)

type TaskUseCase struct {
	repo task.Repository
}

func NewTaskUseCase(repo task.Repository) *TaskUseCase {
	return &TaskUseCase{
		repo: repo,
	}
}

func (m *TaskUseCase) CreateTask(ctx context.Context, incomingValues []string, outgoingValues []string, functionName string) (string, error) {
	taskEntity := &models.Task{
		IncomingValues: incomingValues,
		OutgoingValues: outgoingValues,
		FunctionName:   functionName,
	}
	return m.repo.CreateTask(ctx, taskEntity)
}

func (m *TaskUseCase) GetTask(ctx context.Context, id string) (*models.Task, error) {
	return m.repo.GetTask(ctx, id)
}

func (m *TaskUseCase) UpdateTask(ctx context.Context, id string, outgoingValues []string) (*models.Task, error) {
	return m.repo.UpdateTask(ctx, id, outgoingValues)
}

func (m *TaskUseCase) DeleteTask(ctx context.Context, id string) error {
	return m.repo.DeleteTask(ctx, id)
}
