package models

type ReduceTaskElement struct {
	Value1       string `json:"value1"`
	Value2       string `json:"value2"`
	FunctionName string `json:"function_name"`
}
