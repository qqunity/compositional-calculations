package usecases

import (
	"encoding/json"
	"github.com/Shopify/sarama"
	"github.com/spf13/viper"
	"log"
	"reduce-worker/contractor"
	"reduce-worker/contractor/functions"
	"reduce-worker/kafka"
	"reduce-worker/models"
	"strconv"
)

type ReduceTaskContractorUseCase struct {
	results chan [2]interface{}
}

func NewMapTaskContractorUseCase() *ReduceTaskContractorUseCase {
	return &ReduceTaskContractorUseCase{
		results: make(chan [2]interface{}, 7),
	}
}

func (c *ReduceTaskContractorUseCase) StartConsuming() {
	consumer, err := kafka.ConnectConsumer([]string{viper.GetString("kafka.address")})
	if err != nil {
		log.Fatal(err)
	}
	for i := 0; i < viper.GetInt("kafka.request-partitions-number"); i++ {
		finalI := i
		go func() {
			partitionConsumer, err := consumer.ConsumePartition(viper.GetString("kafka.reduce-topic"), int32(finalI), sarama.OffsetOldest)
			if err != nil {
				log.Fatal(err)
			}
			for {
				select {
				case err := <-partitionConsumer.Errors():
					log.Fatal(err)
				case m := <-partitionConsumer.Messages():
					log.Printf("message received from manager: %s => %s", string(m.Key), string(m.Value))
					key := string(m.Key)
					reduceTaskElement := &models.ReduceTaskElement{}
					err := json.Unmarshal(m.Value, reduceTaskElement)
					if err != nil {
						log.Fatal(err)
					}
					go func() {
						var resultItem [2]interface{}
						resultItem[0] = key
						resultItem[1] = nil
						result, err := contractor.Call(functions.Mapper[reduceTaskElement.FunctionName], reduceTaskElement.Value1, reduceTaskElement.Value2)
						switch v := result.(type) {
						case int:
							result = strconv.Itoa(v)
						}
						if err != nil {
							c.results <- resultItem
							log.Fatalf("can not execute function: %v", err)
						}
						resultItem[1] = result
						c.results <- resultItem
					}()
				}
			}
		}()
	}
}

func (c *ReduceTaskContractorUseCase) StartProducing() {
	for resultItem := range c.results {
		finalResultItem := resultItem
		go func() {
			err := kafka.PushMessageToQueue([]string{viper.GetString("kafka.address")}, viper.GetString("kafka.response-topic"), []byte(finalResultItem[0].(string)), []byte(finalResultItem[1].(string)))
			if err != nil {
				log.Fatal("failed to push message:", err)
			}
		}()
	}
}
