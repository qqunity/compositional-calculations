package contractor

import (
	"errors"
	"reflect"
)

func Call(fun interface{}, params ...interface{}) (interface{}, error) {
	f := reflect.ValueOf(fun)
	if len(params) != f.Type().NumIn() {
		err := errors.New("the number of params is out of index")
		return nil, err
	}
	in := make([]reflect.Value, len(params))
	for k, param := range params {
		in[k] = reflect.ValueOf(param)
	}
	var res []reflect.Value
	res = f.Call(in)
	return res[0].Interface(), nil
}
