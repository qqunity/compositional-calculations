package functions

import "strconv"

var Mapper = map[string]interface{}{
	"Inc": Inc,
	"Dec": Dec,
	"Sum": Sum,
}

func Inc(item interface{}) interface{} {
	switch v := item.(type) {
	case string:
		el, _ := strconv.Atoi(v)
		return el + 1
	default:
		return v.(int) + 1
	}
}

func Dec(item interface{}) interface{} {
	switch v := item.(type) {
	case string:
		el, _ := strconv.Atoi(v)
		return el - 1
	default:
		return v.(int) - 1
	}
}

func Sum(item1, item2 interface{}) interface{} {
	switch v := item1.(type) {
	case string:
		el1, _ := strconv.Atoi(v)
		el2, _ := strconv.Atoi(item2.(string))
		return el1 + el2
	default:
		return v.(int) + item2.(int)
	}
}
