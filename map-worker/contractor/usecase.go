package contractor

type UseCase interface {
	StartConsuming()
	StartProducing()
}
