package usecases

import (
	"encoding/json"
	"github.com/Shopify/sarama"
	"github.com/spf13/viper"
	"log"
	"map-worker/contractor"
	"map-worker/contractor/functions"
	"map-worker/kafka"
	"map-worker/models"
	"strconv"
)

type MapTaskContractorUseCase struct {
	results chan [2]interface{}
}

func NewMapTaskContractorUseCase() *MapTaskContractorUseCase {
	return &MapTaskContractorUseCase{
		results: make(chan [2]interface{}, 7),
	}
}

func (c *MapTaskContractorUseCase) StartConsuming() {
	consumer, err := kafka.ConnectConsumer([]string{viper.GetString("kafka.address")})
	if err != nil {
		log.Fatal(err)
	}
	for i := 0; i < viper.GetInt("kafka.request-partitions-number"); i++ {
		finalI := i
		go func() {
			partitionConsumer, err := consumer.ConsumePartition(viper.GetString("kafka.map-topic"), int32(finalI), sarama.OffsetOldest)
			if err != nil {
				log.Fatal(err)
			}
			for {
				select {
				case err := <-partitionConsumer.Errors():
					log.Fatal(err)
				case m := <-partitionConsumer.Messages():
					log.Printf("message received from manager: %s => %s", string(m.Key), string(m.Value))
					key := string(m.Key)
					mapTaskElement := &models.MapTaskElement{}
					err := json.Unmarshal(m.Value, mapTaskElement)
					if err != nil {
						log.Fatal(err)
					}
					go func() {
						var resultItem [2]interface{}
						resultItem[0] = key
						resultItem[1] = nil
						result, err := contractor.Call(functions.Mapper[mapTaskElement.FunctionName], mapTaskElement.Value)
						switch v := result.(type) {
						case int:
							result = strconv.Itoa(v)
						}
						if err != nil {
							c.results <- resultItem
							log.Fatalf("can not execute function: %v", err)
						}
						resultItem[1] = result
						c.results <- resultItem
					}()
				}
			}
		}()
	}
}

func (c *MapTaskContractorUseCase) StartProducing() {
	for resultItem := range c.results {
		finalResultItem := resultItem
		go func() {
			err := kafka.PushMessageToQueue([]string{viper.GetString("kafka.address")}, viper.GetString("kafka.response-topic"), []byte(finalResultItem[0].(string)), []byte(finalResultItem[1].(string)))
			if err != nil {
				log.Fatal("failed to push message:", err)
			}
		}()
	}
}
