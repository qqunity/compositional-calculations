package main

import (
	"context"
	"golang.org/x/sync/errgroup"
	"log"
	"map-worker/config"
	"map-worker/contractor/usecases"
)

func main() {
	if err := config.Init(); err != nil {
		log.Fatal(err.Error())
	}
	uc := usecases.NewMapTaskContractorUseCase()
	g, _ := errgroup.WithContext(context.Background())
	g.Go(func() error {
		uc.StartConsuming()
		return nil
	})
	g.Go(func() error {
		uc.StartProducing()
		return nil
	})
	if err := g.Wait(); err != nil {
		log.Fatal(err)
	}
}
