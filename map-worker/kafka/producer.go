package kafka

import (
	"github.com/Shopify/sarama"
	"log"
)

func ConnectProducer(brokersUrl []string) (sarama.SyncProducer, error) {

	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = 5

	conn, err := sarama.NewSyncProducer(brokersUrl, config)
	if err != nil {
		return nil, err
	}

	return conn, nil
}

func PushMessageToQueue(brokersUrl []string, topic string, key []byte, value []byte) error {

	producer, err := ConnectProducer(brokersUrl)
	if err != nil {
		return err
	}

	defer func(producer sarama.SyncProducer) {
		err := producer.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(producer)

	msg := &sarama.ProducerMessage{
		Topic: topic,
		Key:   sarama.StringEncoder(key),
		Value: sarama.StringEncoder(value),
	}

	partition, offset, err := producer.SendMessage(msg)
	if err != nil {
		return err
	}

	log.Printf("message after work stored in topic(%s)/partition(%d)/offset(%d): %s", topic, partition, offset, string(key))

	return nil
}
