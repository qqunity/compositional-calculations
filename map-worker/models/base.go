package models

type MapTaskElement struct {
	Value        string `json:"value"`
	FunctionName string `json:"function_name"`
}
