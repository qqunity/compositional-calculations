package models

type MapTask struct {
	ID             string   `json:"id"`
	IncomingValues []string `json:"incoming_values"`
	OutgoingValues []string `json:"outgoing_values"`
	FunctionName   string   `json:"function_name"`
}

type MapTaskElement struct {
	Value        string `json:"value"`
	FunctionName string `json:"function_name"`
}
