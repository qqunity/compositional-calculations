package models

type ReduceTask struct {
	ID             string   `json:"id"`
	IncomingValues []string `json:"incoming_values"`
	OutgoingValue  string   `json:"outgoing_values"`
	FunctionName   string   `json:"function_name"`
}

type ReduceTaskElement struct {
	Value1       string `json:"value1"`
	Value2       string `json:"value2"`
	FunctionName string `json:"function_name"`
}
