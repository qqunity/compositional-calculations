// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package delivery

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// ExecutorClient is the client API for Executor service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ExecutorClient interface {
	CompleteMapTask(ctx context.Context, in *CompleteMapTaskRequest, opts ...grpc.CallOption) (*CompleteMapTaskResponse, error)
	CompleteReduceTask(ctx context.Context, in *CompleteReduceTaskRequest, opts ...grpc.CallOption) (*CompleteReduceTaskResponse, error)
}

type executorClient struct {
	cc grpc.ClientConnInterface
}

func NewExecutorClient(cc grpc.ClientConnInterface) ExecutorClient {
	return &executorClient{cc}
}

func (c *executorClient) CompleteMapTask(ctx context.Context, in *CompleteMapTaskRequest, opts ...grpc.CallOption) (*CompleteMapTaskResponse, error) {
	out := new(CompleteMapTaskResponse)
	err := c.cc.Invoke(ctx, "/delivery.Executor/CompleteMapTask", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *executorClient) CompleteReduceTask(ctx context.Context, in *CompleteReduceTaskRequest, opts ...grpc.CallOption) (*CompleteReduceTaskResponse, error) {
	out := new(CompleteReduceTaskResponse)
	err := c.cc.Invoke(ctx, "/delivery.Executor/CompleteReduceTask", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ExecutorServer is the server API for Executor service.
// All implementations must embed UnimplementedExecutorServer
// for forward compatibility
type ExecutorServer interface {
	CompleteMapTask(context.Context, *CompleteMapTaskRequest) (*CompleteMapTaskResponse, error)
	CompleteReduceTask(context.Context, *CompleteReduceTaskRequest) (*CompleteReduceTaskResponse, error)
	mustEmbedUnimplementedExecutorServer()
}

// UnimplementedExecutorServer must be embedded to have forward compatible implementations.
type UnimplementedExecutorServer struct {
}

func (UnimplementedExecutorServer) CompleteMapTask(context.Context, *CompleteMapTaskRequest) (*CompleteMapTaskResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CompleteMapTask not implemented")
}
func (UnimplementedExecutorServer) CompleteReduceTask(context.Context, *CompleteReduceTaskRequest) (*CompleteReduceTaskResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CompleteReduceTask not implemented")
}
func (UnimplementedExecutorServer) mustEmbedUnimplementedExecutorServer() {}

// UnsafeExecutorServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ExecutorServer will
// result in compilation errors.
type UnsafeExecutorServer interface {
	mustEmbedUnimplementedExecutorServer()
}

func RegisterExecutorServer(s grpc.ServiceRegistrar, srv ExecutorServer) {
	s.RegisterService(&Executor_ServiceDesc, srv)
}

func _Executor_CompleteMapTask_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CompleteMapTaskRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ExecutorServer).CompleteMapTask(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/delivery.Executor/CompleteMapTask",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ExecutorServer).CompleteMapTask(ctx, req.(*CompleteMapTaskRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Executor_CompleteReduceTask_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CompleteReduceTaskRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ExecutorServer).CompleteReduceTask(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/delivery.Executor/CompleteReduceTask",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ExecutorServer).CompleteReduceTask(ctx, req.(*CompleteReduceTaskRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// Executor_ServiceDesc is the grpc.ServiceDesc for Executor service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Executor_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "delivery.Executor",
	HandlerType: (*ExecutorServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CompleteMapTask",
			Handler:    _Executor_CompleteMapTask_Handler,
		},
		{
			MethodName: "CompleteReduceTask",
			Handler:    _Executor_CompleteReduceTask_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "executor.proto",
}
