package task

import "context"

type UseCase interface {
	CompleteMapTask(ctx context.Context, id string) error
	CompleteReduceTask(ctx context.Context, id string) error
}
