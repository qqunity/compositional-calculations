package delivery

import (
	"context"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"manager/models"
	"strconv"
	"time"
)

func (t *Task) toReduceTaskModel() *models.ReduceTask {
	if len(t.OutgoingValues) > 0 {
		return &models.ReduceTask{
			ID:             t.Id,
			OutgoingValue:  t.OutgoingValues[0],
			IncomingValues: t.IncomingValues,
			FunctionName:   t.FunctionName,
		}
	} else {
		return &models.ReduceTask{
			ID:             t.Id,
			OutgoingValue:  "",
			IncomingValues: t.IncomingValues,
			FunctionName:   t.FunctionName,
		}
	}

}

func reduceTaskToProtoModel(r *models.ReduceTask) *Task {
	return &Task{
		Id:             r.ID,
		OutgoingValues: []string{r.OutgoingValue},
		IncomingValues: r.IncomingValues,
		FunctionName:   r.FunctionName,
	}
}

type ReduceTaskStorageClient struct {
	addr string
}

func NewReduceTaskStorageClient(addr string) *ReduceTaskStorageClient {
	return &ReduceTaskStorageClient{
		addr: addr,
	}
}

func (s *ReduceTaskStorageClient) CreateReduceTask(ctx context.Context, reduceTask *models.ReduceTask) (string, error) {
	conn, err := grpc.Dial(s.addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return "", err
	}

	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			log.Fatalf("error closing connection: %v", err)
		}
	}(conn)
	c := NewStorageClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(viper.GetInt("storage-client.timeout"))*time.Second)
	defer cancel()

	r, err := c.CreateTask(ctx, &CreateTaskRequest{
		Task: reduceTaskToProtoModel(reduceTask),
	})
	if err != nil {
		log.Fatalf("could not create reduce task: %v", err)
		return "", err
	}
	return r.Id, nil
}

func (s *ReduceTaskStorageClient) GetReduceTask(ctx context.Context, id string) (*models.ReduceTask, error) {
	conn, err := grpc.Dial(s.addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return nil, err
	}

	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			log.Fatalf("error closing connection: %v", err)
		}
	}(conn)
	c := NewStorageClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(viper.GetInt("storage-client.timeout"))*time.Second)
	defer cancel()
	r, err := c.GetTask(ctx, &GetTaskRequest{
		Id: id,
	})
	if err != nil {
		log.Fatalf("could not get reduce task: %v", err)
		return nil, err
	}
	return r.Task.toReduceTaskModel(), nil
}

func (s *ReduceTaskStorageClient) UpdateReduceTask(ctx context.Context, id string, outgoingValue interface{}) (*models.ReduceTask, error) {
	conn, err := grpc.Dial(s.addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return nil, err
	}

	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			log.Fatalf("error closing connection: %v", err)
		}
	}(conn)
	c := NewStorageClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(viper.GetInt("storage-client.timeout"))*time.Second)
	defer cancel()
	convertedOutgoingValues := make([]string, 0, 1)
	if outgoingValue != nil {
		switch v := outgoingValue.(type) {
		case int:
			convertedOutgoingValues = append(convertedOutgoingValues, strconv.Itoa(v))
		default:
			convertedOutgoingValues = append(convertedOutgoingValues, v.(string))
		}
	}
	r, err := c.UpdateTask(ctx, &UpdateTaskRequest{
		Id:             id,
		OutgoingValues: convertedOutgoingValues,
	})
	if err != nil {
		log.Fatalf("could not update reduce task: %v", err)
		return nil, err
	}
	return r.Task.toReduceTaskModel(), nil
}

func (s *ReduceTaskStorageClient) DeleteReduceTask(ctx context.Context, id string) error {
	conn, err := grpc.Dial(s.addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return err
	}

	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			log.Fatalf("error closing connection: %v", err)
		}
	}(conn)
	c := NewStorageClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(viper.GetInt("storage-client.timeout"))*time.Second)
	defer cancel()
	_, err = c.DeleteTask(ctx, &DeleteTaskRequest{
		Id: id,
	})
	if err != nil {
		log.Fatalf("could not delete reduce task: %v", err)
		return err
	}
	return nil
}
