package usecases

import (
	"context"
	"encoding/json"
	"github.com/Shopify/sarama"
	"github.com/spf13/viper"
	"golang.org/x/sync/errgroup"
	"log"
	"manager/kafka"
	"manager/models"
	"manager/task"
	"sort"
	"strconv"
	"strings"
)

type TaskExecutorUseCase struct {
	mapRepo    task.MapTaskRepository
	reduceRepo task.ReduceTaskRepository
	results    map[string]chan interface{}
}

func NewTaskExecutorUseCase(mr task.MapTaskRepository, rr task.ReduceTaskRepository) *TaskExecutorUseCase {
	e := &TaskExecutorUseCase{
		mapRepo:    mr,
		reduceRepo: rr,
		results:    make(map[string]chan interface{}),
	}
	go e.startConsuming()
	return e
}

func (e *TaskExecutorUseCase) startConsuming() {
	consumer, err := kafka.ConnectConsumer([]string{viper.GetString("kafka.address")})
	if err != nil {
		log.Fatal(err)
	}
	for i := 0; i < viper.GetInt("kafka.response-partitions-number"); i++ {
		finalI := i
		go func() {
			partitionConsumer, err := consumer.ConsumePartition(viper.GetString("kafka.response-topic"), int32(finalI), sarama.OffsetOldest)
			if err != nil {
				log.Fatal(err)
			}
			for {
				select {
				case err := <-partitionConsumer.Errors():
					log.Fatal(err)
				case m := <-partitionConsumer.Messages():
					log.Printf("message received from worker: %s => %s", string(m.Key), string(m.Value))
					key := string(m.Key)
					keyParts := strings.Split(key, "_")
					if _, isExist := e.results[keyParts[0]]; isExist {
						if len(keyParts) == 2 {
							e.results[keyParts[0]] <- string(m.Value) + "_" + keyParts[1]

						} else {
							e.results[keyParts[0]] <- string(m.Value)
						}
					}
				}
			}
		}()
	}
}

type byOrder []interface{}

func (s byOrder) Len() int {
	return len(s)
}

func (s byOrder) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
func (s byOrder) Less(i, j int) bool {
	sIParts := strings.Split(s[i].(string), "_")
	sJParts := strings.Split(s[j].(string), "_")
	posI, _ := strconv.Atoi(sIParts[1])
	posJ, _ := strconv.Atoi(sJParts[1])
	return posI < posJ
}

func (e *TaskExecutorUseCase) CompleteMapTask(ctx context.Context, id string) error {
	mapTask, err := e.mapRepo.GetMapTask(ctx, id)
	if err != nil {
		log.Fatal("failed to get map task:", err)
		return err
	}
	e.results[mapTask.ID] = make(chan interface{}, 7)
	cnt := len(mapTask.IncomingValues)
	g, _ := errgroup.WithContext(ctx)
	for i, value := range mapTask.IncomingValues {
		mapTaskElement := models.MapTaskElement{
			Value:        value,
			FunctionName: mapTask.FunctionName,
		}
		mapTaskElementRaw, err := json.Marshal(mapTaskElement)
		if err != nil {
			log.Fatal("failed to get map task element bytes:", err)
			return err
		}
		finalI := i
		g.Go(func() error {
			err := kafka.PushMessageToQueue([]string{viper.GetString("kafka.address")}, viper.GetString("kafka.map-topic"), []byte(mapTask.ID+"_"+strconv.Itoa(finalI)), mapTaskElementRaw)
			if err != nil {
				log.Fatal("failed to push message:", err)
				return err
			}
			return nil
		})
	}
	if err := g.Wait(); err != nil {
		return err
	}
	rawOutgoingValues := make([]interface{}, 0, 3)
	for cnt > 0 {
		result := <-e.results[mapTask.ID]
		rawOutgoingValues = append(rawOutgoingValues, result)
		cnt--
	}
	log.Printf("completed task with id: %s", mapTask.ID)
	sort.Sort(byOrder(rawOutgoingValues))
	outgoingValues := make([]interface{}, 0, 1)
	for _, outgoingValue := range rawOutgoingValues {
		parts := strings.Split(outgoingValue.(string), "_")
		outgoingValues = append(outgoingValues, parts[0])
	}
	_, err = e.mapRepo.UpdateMapTask(ctx, id, outgoingValues)
	if err != nil {
		return err
	}
	return nil
}

func (e *TaskExecutorUseCase) CompleteReduceTask(ctx context.Context, id string) error {
	reduceTask, err := e.reduceRepo.GetReduceTask(ctx, id)
	if err != nil {
		log.Fatal("failed to get reduce task:", err)
		return err
	}
	e.results[reduceTask.ID] = make(chan interface{})
	reduceTaskElement := models.ReduceTaskElement{
		Value1:       reduceTask.IncomingValues[0],
		Value2:       reduceTask.IncomingValues[1],
		FunctionName: reduceTask.FunctionName,
	}
	reduceTaskElementRaw, err := json.Marshal(reduceTaskElement)
	if err != nil {
		log.Fatal("failed to get reduce task element bytes:", err)
		return err
	}
	err = kafka.PushMessageToQueue([]string{viper.GetString("kafka.address")}, viper.GetString("kafka.reduce-topic"), []byte(reduceTask.ID), reduceTaskElementRaw)
	if err != nil {
		log.Fatal("failed to push message:", err)
		return err
	}
	result := <-e.results[reduceTask.ID]
	for i := 2; i < len(reduceTask.IncomingValues); i++ {
		reduceTaskElement = models.ReduceTaskElement{
			Value1:       result.(string),
			Value2:       reduceTask.IncomingValues[i],
			FunctionName: reduceTask.FunctionName,
		}
		reduceTaskElementRaw, err = json.Marshal(reduceTaskElement)
		if err != nil {
			log.Fatal("failed to get reduce task element bytes:", err)
			return err
		}
		err = kafka.PushMessageToQueue([]string{viper.GetString("kafka.address")}, viper.GetString("kafka.reduce-topic"), []byte(reduceTask.ID), reduceTaskElementRaw)
		if err != nil {
			log.Fatal("failed to push message:", err)
			return err
		}
		result = <-e.results[reduceTask.ID]
	}
	_, err = e.reduceRepo.UpdateReduceTask(ctx, id, result)
	if err != nil {
		return err
	}
	return nil
}
