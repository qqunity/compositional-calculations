package main

import (
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"log"
	"manager/config"
	mapTask "manager/server"
	pb "manager/task/delivery"
	"manager/task/repository/delivery"
	"manager/task/usecases"
	"net"
)

func main() {
	if err := config.Init(); err != nil {
		log.Fatal(err.Error())
	}
	lis, err := net.Listen("tcp", viper.GetString("app.address"))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer([]grpc.ServerOption{}...)

	server := &mapTask.TaskServer{
		UseCase: usecases.NewTaskExecutorUseCase(delivery.NewMapTaskStorageClient(viper.GetString("storage-client.address")), delivery.NewReduceTaskStorageClient(viper.GetString("storage-client.address"))),
	}

	pb.RegisterExecutorServer(grpcServer, server)

	log.Printf("starting server on address %s", lis.Addr().String())

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
}
