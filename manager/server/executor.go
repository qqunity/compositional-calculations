package server

import (
	"context"
	"log"
	"manager/task"
	pb "manager/task/delivery"
)

type TaskServer struct {
	pb.UnimplementedExecutorServer
	UseCase task.UseCase
}

func (s *TaskServer) CompleteMapTask(ctx context.Context, r *pb.CompleteMapTaskRequest) (*pb.CompleteMapTaskResponse, error) {
	resp := &pb.CompleteMapTaskResponse{}
	err := s.UseCase.CompleteMapTask(ctx, r.Id)
	if err != nil {
		return resp, err
	}
	log.Printf("complete map task request: %v", r)
	return resp, nil
}

func (s *TaskServer) CompleteReduceTask(ctx context.Context, r *pb.CompleteReduceTaskRequest) (*pb.CompleteReduceTaskResponse, error) {
	resp := &pb.CompleteReduceTaskResponse{}
	err := s.UseCase.CompleteReduceTask(ctx, r.Id)
	if err != nil {
		return resp, err
	}
	log.Printf("complete reduce task request: %v", r)
	return resp, nil
}
