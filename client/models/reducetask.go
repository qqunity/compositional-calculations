package models

type ReduceTask struct {
	ID             string
	IncomingValues []interface{}
	OutgoingValue  interface{}
	FunctionName   string
}
