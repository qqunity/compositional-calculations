package models

type Job func(in, out chan interface{}) error
