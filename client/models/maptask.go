package models

type MapTask struct {
	ID             string
	IncomingValues []interface{}
	OutgoingValues []interface{}
	FunctionName   string
}
