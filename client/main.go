package main

import (
	"client/config"
	"client/executor/functions"
	"client/executor/repository/delivery"
	"context"
	"fmt"
	"github.com/spf13/viper"
	"log"
	"time"
)

func main() {
	if err := config.Init(); err != nil {
		log.Fatal(err.Error())
	}
	exp := &functions.Expression{}
	mapRepo := delivery.NewMapTaskStorageClient(
		viper.GetString("storage-client.address"),
	)
	reduceRepo := delivery.NewReduceTaskStorageClient(
		viper.GetString("storage-client.address"),
	)
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	values := make([]interface{}, 0)
	for i := 1; i <= 1000; i++ {
		values = append(values, i)
	}
	res, err := exp.Init(ctx, mapRepo, reduceRepo, values...).Reduce(functions.Sum).Result()
	//res, err := exp.Init(ctx, mapRepo, reduceRepo, values...).Map(functions.Inc).Map(functions.Inc).Result()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(res)
}
