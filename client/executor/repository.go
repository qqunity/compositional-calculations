package executor

import (
	"client/models"
	"context"
)

type MapTaskRepository interface {
	CreateMapTask(ctx context.Context, mapTask *models.MapTask) (string, error)
	GetMapTask(ctx context.Context, id string) (*models.MapTask, error)
	UpdateMapTask(ctx context.Context, id string, outgoingValues []interface{}) (*models.MapTask, error)
	DeleteMapTask(ctx context.Context, id string) error
}

type ReduceTaskRepository interface {
	CreateReduceTask(ctx context.Context, reduceTask *models.ReduceTask) (string, error)
	GetReduceTask(ctx context.Context, id string) (*models.ReduceTask, error)
	UpdateReduceTask(ctx context.Context, id string, outgoingValue interface{}) (*models.ReduceTask, error)
	DeleteReduceTask(ctx context.Context, id string) error
}
