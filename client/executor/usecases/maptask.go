package usecases

import (
	"client/executor"
	"client/executor/delivery"
	"client/models"
	"context"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"reflect"
	"runtime"
	"strings"
	"time"
)

type MapTaskUseCase struct {
	repo     executor.MapTaskRepository
	Function interface{}
}

func NewMapTaskUseCase(f interface{}, repo executor.MapTaskRepository) *MapTaskUseCase {
	return &MapTaskUseCase{
		repo:     repo,
		Function: f,
	}
}

func (m *MapTaskUseCase) Job(ctx context.Context) (models.Job, error) {
	job := func(in, out chan interface{}) error {
		incomingValues := make([]interface{}, 0)
		log.Println("pre start map")
		for value := range in {
			incomingValues = append(incomingValues, value)
		}
		log.Println("start map")
		funcNameParts := strings.Split(runtime.FuncForPC(reflect.ValueOf(m.Function).Pointer()).Name(), ".")
		id, err := m.repo.CreateMapTask(ctx, &models.MapTask{
			IncomingValues: incomingValues,
			FunctionName:   funcNameParts[1],
		})
		log.Println("creating map task with id:", id)
		if err != nil {
			return err
		}
		conn, err := grpc.Dial(viper.GetString("manager.address"), grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			log.Fatalf("did not connect: %v", err)
			return err
		}
		defer func(conn *grpc.ClientConn) {
			err := conn.Close()
			if err != nil {
				log.Fatalf("error closing connection: %v", err)
			}
		}(conn)
		c := delivery.NewExecutorClient(conn)
		rCtx, cancel := context.WithTimeout(context.Background(), time.Duration(viper.GetInt("manager.timeout"))*time.Second)
		defer cancel()
		_, err = c.CompleteMapTask(rCtx, &delivery.CompleteMapTaskRequest{
			Id: id,
		})
		if err != nil {
			log.Fatalf("could not get map task: %v", err)
			return err
		}
		//time.Sleep(3 * time.Second)
		mapTask, err := m.repo.GetMapTask(ctx, id)
		log.Println("completing map task with id:", id)
		if err != nil {
			return err
		}
		for _, value := range mapTask.OutgoingValues {
			out <- value
		}
		log.Println("end map")
		return nil
	}
	return job, nil
}
