package usecases

import (
	"client/executor"
	"client/executor/delivery"
	"client/models"
	"context"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"reflect"
	"runtime"
	"strings"
	"time"
)

type ReduceTaskUseCase struct {
	repo     executor.ReduceTaskRepository
	Function interface{}
}

func NewReduceTaskUseCase(f interface{}, repo executor.ReduceTaskRepository) *ReduceTaskUseCase {
	return &ReduceTaskUseCase{
		repo:     repo,
		Function: f,
	}
}

func (r *ReduceTaskUseCase) Job(ctx context.Context) (models.Job, error) {
	job := func(in, out chan interface{}) error {
		incomingValues := make([]interface{}, 0)
		log.Println("pre start reduce")
		for value := range in {
			incomingValues = append(incomingValues, value)
		}
		log.Println("start reduce")
		funcNameParts := strings.Split(runtime.FuncForPC(reflect.ValueOf(r.Function).Pointer()).Name(), ".")
		id, err := r.repo.CreateReduceTask(ctx, &models.ReduceTask{
			IncomingValues: incomingValues,
			FunctionName:   funcNameParts[1],
		})
		log.Println("creating reduce task with id:", id)
		if err != nil {
			return err
		}
		conn, err := grpc.Dial(viper.GetString("manager.address"), grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			log.Fatalf("did not connect: %v", err)
			return err
		}
		defer func(conn *grpc.ClientConn) {
			err := conn.Close()
			if err != nil {
				log.Fatalf("error closing connection: %v", err)
			}
		}(conn)
		c := delivery.NewExecutorClient(conn)
		rCtx, cancel := context.WithTimeout(context.Background(), time.Duration(viper.GetInt("manager.timeout"))*time.Second)
		defer cancel()
		_, err = c.CompleteReduceTask(rCtx, &delivery.CompleteReduceTaskRequest{
			Id: id,
		})
		if err != nil {
			log.Fatalf("could not get reduce task: %v", err)
			return err
		}
		reduceTask, err := r.repo.GetReduceTask(ctx, id)
		log.Println("completing reduce task with id:", id)
		if err != nil {
			return err
		}
		out <- reduceTask.OutgoingValue
		log.Println("end reduce")
		return nil
	}
	return job, nil
}
