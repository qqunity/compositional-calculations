package usecases

import (
	"client/models"
	"context"
	"golang.org/x/sync/errgroup"
)

type TaskUseCase interface {
	Job(ctx context.Context) (models.Job, error)
}

type ExecutorUseCase struct {
	items       []interface{}
	resultItems []interface{}
}

func NewExecutorUseCase(items ...interface{}) *ExecutorUseCase {
	return &ExecutorUseCase{
		items:       items,
		resultItems: make([]interface{}, 0, 1),
	}
}

func (e *ExecutorUseCase) GetItems() []interface{} {
	return e.resultItems
}

func (e *ExecutorUseCase) Run(ctx context.Context, jobs ...models.Job) error {
	jobs = append([]models.Job{func(in, out chan interface{}) error {
		for _, item := range e.items {
			out <- item
		}
		return nil
	}}, jobs...)
	jobs = append(jobs, func(in, out chan interface{}) error {
		for item := range in {
			e.resultItems = append(e.resultItems, item)
		}
		return nil
	})
	g, _ := errgroup.WithContext(ctx)
	in := make(chan interface{}, 5)
	out := make(chan interface{}, 5)
	for _, job := range jobs {
		finalJob := job
		finalIn := in
		finalOut := out
		g.Go(func() error {
			defer close(finalOut)
			err := finalJob(finalIn, finalOut)
			return err
		})
		in = out
		out = make(chan interface{}, 5)
	}
	go func() {
		_ = g.Wait()
	}()

	if err := g.Wait(); err != nil {
		return err
	}
	return nil
}
