package executor

import "errors"

var (
	ErrMapTaskNotFound = errors.New("map task not found")
)
