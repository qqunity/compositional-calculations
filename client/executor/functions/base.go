package functions

import (
	"client/executor"
	"client/executor/usecases"
	"client/models"
	"context"
	"reflect"
	"runtime"
	"strings"
	"sync"
)

type Expression struct {
	values     []interface{}
	executor   *usecases.ExecutorUseCase
	jobs       []models.Job
	mutex      sync.Mutex
	err        error
	ctx        context.Context
	mapRepo    executor.MapTaskRepository
	reduceRepo executor.ReduceTaskRepository
}

func (e *Expression) Init(ctx context.Context, mapRepo executor.MapTaskRepository, reduceRepo executor.ReduceTaskRepository, values ...interface{}) *Expression {
	defer e.mutex.Unlock()
	e.mutex.Lock()
	e.values = values
	e.executor = usecases.NewExecutorUseCase(values...)
	e.jobs = make([]models.Job, 0, 3)
	e.ctx = ctx
	e.mapRepo = mapRepo
	e.reduceRepo = reduceRepo
	return e
}

func (e *Expression) Map(f func(interface{}) interface{}) *Expression {
	if e.err != nil {
		panic(e.err)
	}
	funcNameParts := strings.Split(runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name(), ".")
	mapTask := usecases.NewMapTaskUseCase(
		Mapper[funcNameParts[1]],
		e.mapRepo,
	)
	job, err := mapTask.Job(e.ctx)
	if err != nil {
		e.err = err
	}
	e.jobs = append(e.jobs, job)
	return e
}

func (e *Expression) Reduce(f func(interface{}, interface{}) interface{}) *Expression {
	if e.err != nil {
		panic(e.err)
	}
	funcNameParts := strings.Split(runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name(), ".")
	reduceTask := usecases.NewReduceTaskUseCase(
		Mapper[funcNameParts[1]],
		e.reduceRepo,
	)
	job, err := reduceTask.Job(e.ctx)
	if err != nil {
		e.err = err
	}
	e.jobs = append(e.jobs, job)
	return e
}

func (e *Expression) Result() ([]interface{}, error) {
	if e.err != nil {
		panic(e.err)
	}
	err := e.executor.Run(e.ctx, e.jobs...)
	if err != nil {
		return nil, err
	}
	return e.executor.GetItems(), nil
}
