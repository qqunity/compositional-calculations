package executor

import (
	"client/models"
	"context"
)

type UseCase interface {
	Run(ctx context.Context, jobs ...models.Job) error
}
