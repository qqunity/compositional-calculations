package localstorage

import (
	"client/executor"
	"client/models"
	"context"
	"github.com/google/uuid"
	"sync"
)

type MapTaskLocalStorage struct {
	tasks map[string]*models.MapTask
	mutex sync.Mutex
}

func NewMapLocalStorage() *MapTaskLocalStorage {
	return &MapTaskLocalStorage{
		tasks: make(map[string]*models.MapTask),
	}
}

func (l *MapTaskLocalStorage) CreateMapTask(ctx context.Context, mapTask *models.MapTask) (string, error) {
	defer l.mutex.Unlock()
	l.mutex.Lock()
	if mapTask.ID == "" {
		id := uuid.New()
		mapTask.ID = id.String()
	}
	l.tasks[mapTask.ID] = mapTask
	return mapTask.ID, nil
}

func (l *MapTaskLocalStorage) GetMapTask(ctx context.Context, id string) (*models.MapTask, error) {
	defer l.mutex.Unlock()
	l.mutex.Lock()
	if mapTask, isExist := l.tasks[id]; isExist {
		return mapTask, nil
	}
	return nil, executor.ErrMapTaskNotFound
}

func (l *MapTaskLocalStorage) UpdateMapTask(ctx context.Context, id string, outgoingValues []interface{}) (*models.MapTask, error) {
	defer l.mutex.Unlock()
	l.mutex.Lock()
	if mapTask, isExist := l.tasks[id]; isExist {
		mapTask.OutgoingValues = outgoingValues
		return mapTask, nil
	}
	return nil, executor.ErrMapTaskNotFound
}

func (l *MapTaskLocalStorage) DeleteMapTask(ctx context.Context, id string) error {
	defer l.mutex.Unlock()
	l.mutex.Lock()
	delete(l.tasks, id)
	return nil
}
