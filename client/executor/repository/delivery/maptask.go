package delivery

import (
	"client/models"
	"context"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"strconv"
	"time"
)

func (t *Task) toMapTaskModel() *models.MapTask {
	incomingValues := make([]interface{}, 0, 4)
	for _, value := range t.IncomingValues {
		incomingValues = append(incomingValues, value)
	}
	outgoingValues := make([]interface{}, 0, 4)
	for _, value := range t.OutgoingValues {
		outgoingValues = append(outgoingValues, value)
	}
	return &models.MapTask{
		ID:             t.Id,
		OutgoingValues: outgoingValues,
		IncomingValues: incomingValues,
		FunctionName:   t.FunctionName,
	}
}

func mapTaskToProtoModel(m *models.MapTask) *Task {
	incomingValues := make([]string, 0, 4)
	for _, value := range m.IncomingValues {
		switch v := value.(type) {
		case int:
			incomingValues = append(incomingValues, strconv.Itoa(v))
		default:
			incomingValues = append(incomingValues, v.(string))
		}
	}
	outgoingValues := make([]string, 0, 4)
	for _, value := range m.OutgoingValues {
		switch v := value.(type) {
		case int:
			outgoingValues = append(outgoingValues, strconv.Itoa(v))
		default:
			outgoingValues = append(outgoingValues, v.(string))
		}
	}
	return &Task{
		Id:             m.ID,
		OutgoingValues: outgoingValues,
		IncomingValues: incomingValues,
		FunctionName:   m.FunctionName,
	}
}

type MapTaskStorageClient struct {
	addr string
}

func NewMapTaskStorageClient(addr string) *MapTaskStorageClient {
	return &MapTaskStorageClient{
		addr: addr,
	}
}

func (s *MapTaskStorageClient) CreateMapTask(ctx context.Context, mapTask *models.MapTask) (string, error) {
	conn, err := grpc.Dial(s.addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return "", err
	}

	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			log.Fatalf("error closing connection: %v", err)
		}
	}(conn)
	c := NewStorageClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(viper.GetInt("storage-client.timeout"))*time.Second)
	defer cancel()

	r, err := c.CreateTask(ctx, &CreateTaskRequest{
		Task: mapTaskToProtoModel(mapTask),
	})
	if err != nil {
		log.Fatalf("could not create map task: %v", err)
		return "", err
	}
	return r.Id, nil
}

func (s *MapTaskStorageClient) GetMapTask(ctx context.Context, id string) (*models.MapTask, error) {
	conn, err := grpc.Dial(s.addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return nil, err
	}

	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			log.Fatalf("error closing connection: %v", err)
		}
	}(conn)
	c := NewStorageClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(viper.GetInt("storage-client.timeout"))*time.Second)
	defer cancel()
	r, err := c.GetTask(ctx, &GetTaskRequest{
		Id: id,
	})
	if err != nil {
		log.Fatalf("could not get map task: %v", err)
		return nil, err
	}
	return r.Task.toMapTaskModel(), nil
}

func (s *MapTaskStorageClient) UpdateMapTask(ctx context.Context, id string, outgoingValues []interface{}) (*models.MapTask, error) {
	conn, err := grpc.Dial(s.addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return nil, err
	}

	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			log.Fatalf("error closing connection: %v", err)
		}
	}(conn)
	c := NewStorageClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(viper.GetInt("storage-client.timeout"))*time.Second)
	defer cancel()
	convertedOutgoingValues := make([]string, 0, 4)
	for _, value := range outgoingValues {
		switch v := value.(type) {
		case int:
			convertedOutgoingValues = append(convertedOutgoingValues, strconv.Itoa(v))
		default:
			convertedOutgoingValues = append(convertedOutgoingValues, v.(string))
		}
	}
	r, err := c.UpdateTask(ctx, &UpdateTaskRequest{
		Id:             id,
		OutgoingValues: convertedOutgoingValues,
	})
	if err != nil {
		log.Fatalf("could not update map task: %v", err)
		return nil, err
	}
	return r.Task.toMapTaskModel(), nil
}

func (s *MapTaskStorageClient) DeleteMapTask(ctx context.Context, id string) error {
	conn, err := grpc.Dial(s.addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return err
	}

	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			log.Fatalf("error closing connection: %v", err)
		}
	}(conn)
	c := NewStorageClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(viper.GetInt("storage-client.timeout"))*time.Second)
	defer cancel()
	_, err = c.DeleteTask(ctx, &DeleteTaskRequest{
		Id: id,
	})
	if err != nil {
		log.Fatalf("could not delete map task: %v", err)
		return err
	}
	return nil
}
