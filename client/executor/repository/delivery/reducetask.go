package delivery

import (
	"client/models"
	"context"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"strconv"
	"time"
)

func (t *Task) toReduceTaskModel() *models.ReduceTask {
	incomingValues := make([]interface{}, 0, 4)
	for _, value := range t.IncomingValues {
		incomingValues = append(incomingValues, value)
	}
	outgoingValues := make([]interface{}, 0, 4)
	for _, value := range t.OutgoingValues {
		outgoingValues = append(outgoingValues, value)
	}
	if len(outgoingValues) > 0 {
		return &models.ReduceTask{
			ID:             t.Id,
			OutgoingValue:  outgoingValues[0],
			IncomingValues: incomingValues,
			FunctionName:   t.FunctionName,
		}
	} else {
		return &models.ReduceTask{
			ID:             t.Id,
			OutgoingValue:  "",
			IncomingValues: incomingValues,
			FunctionName:   t.FunctionName,
		}
	}

}

func reduceTaskToProtoModel(r *models.ReduceTask) *Task {
	incomingValues := make([]string, 0, 4)
	for _, value := range r.IncomingValues {
		switch v := value.(type) {
		case int:
			incomingValues = append(incomingValues, strconv.Itoa(v))
		default:
			incomingValues = append(incomingValues, v.(string))
		}
	}
	outgoingValues := make([]string, 0, 4)
	if r.OutgoingValue != nil {
		switch v := r.OutgoingValue.(type) {
		case int:
			outgoingValues = append(outgoingValues, strconv.Itoa(v))
		default:
			outgoingValues = append(outgoingValues, v.(string))
		}
	}
	return &Task{
		Id:             r.ID,
		OutgoingValues: outgoingValues,
		IncomingValues: incomingValues,
		FunctionName:   r.FunctionName,
	}
}

type ReduceTaskStorageClient struct {
	addr string
}

func NewReduceTaskStorageClient(addr string) *ReduceTaskStorageClient {
	return &ReduceTaskStorageClient{
		addr: addr,
	}
}

func (s *ReduceTaskStorageClient) CreateReduceTask(ctx context.Context, reduceTask *models.ReduceTask) (string, error) {
	conn, err := grpc.Dial(s.addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return "", err
	}

	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			log.Fatalf("error closing connection: %v", err)
		}
	}(conn)
	c := NewStorageClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(viper.GetInt("storage-client.timeout"))*time.Second)
	defer cancel()

	r, err := c.CreateTask(ctx, &CreateTaskRequest{
		Task: reduceTaskToProtoModel(reduceTask),
	})
	if err != nil {
		log.Fatalf("could not create map task: %v", err)
		return "", err
	}
	return r.Id, nil
}

func (s *ReduceTaskStorageClient) GetReduceTask(ctx context.Context, id string) (*models.ReduceTask, error) {
	conn, err := grpc.Dial(s.addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return nil, err
	}

	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			log.Fatalf("error closing connection: %v", err)
		}
	}(conn)
	c := NewStorageClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(viper.GetInt("storage-client.timeout"))*time.Second)
	defer cancel()
	r, err := c.GetTask(ctx, &GetTaskRequest{
		Id: id,
	})
	if err != nil {
		log.Fatalf("could not get map task: %v", err)
		return nil, err
	}
	return r.Task.toReduceTaskModel(), nil
}

func (s *ReduceTaskStorageClient) UpdateReduceTask(ctx context.Context, id string, outgoingValue interface{}) (*models.ReduceTask, error) {
	conn, err := grpc.Dial(s.addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return nil, err
	}

	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			log.Fatalf("error closing connection: %v", err)
		}
	}(conn)
	c := NewStorageClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(viper.GetInt("storage-client.timeout"))*time.Second)
	defer cancel()
	convertedOutgoingValues := make([]string, 0, 4)
	if outgoingValue != nil {
		switch v := outgoingValue.(type) {
		case int:
			convertedOutgoingValues = append(convertedOutgoingValues, strconv.Itoa(v))
		default:
			convertedOutgoingValues = append(convertedOutgoingValues, v.(string))
		}
	}
	r, err := c.UpdateTask(ctx, &UpdateTaskRequest{
		Id:             id,
		OutgoingValues: convertedOutgoingValues,
	})
	if err != nil {
		log.Fatalf("could not update map task: %v", err)
		return nil, err
	}
	return r.Task.toReduceTaskModel(), nil
}

func (s *ReduceTaskStorageClient) DeleteReduceTask(ctx context.Context, id string) error {
	conn, err := grpc.Dial(s.addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return err
	}

	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			log.Fatalf("error closing connection: %v", err)
		}
	}(conn)
	c := NewStorageClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(viper.GetInt("storage-client.timeout"))*time.Second)
	defer cancel()
	_, err = c.DeleteTask(ctx, &DeleteTaskRequest{
		Id: id,
	})
	if err != nil {
		log.Fatalf("could not delete map task: %v", err)
		return err
	}
	return nil
}
